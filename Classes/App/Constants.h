// DEBUG MODE - set as compile flag in build settings
#ifdef DEBUG
#define USE_TESTING_ADS
#endif


// TODO: Metro RSS Feed
#define kRssUrlBase                 @"http://tastespace.wordpress.com/feed/"   //@"http://testxtreme.wordpress.com/feed/"


// Metro screen names
#define kLatestNews                 @"Latest News"

// TODO: Flurry Accounts 
#define	kiPhoneFlurryAccount        @"metro_news_iphone"
#define	kiPadFlurryAccount          @"metro_news_ipad"


// Twitter endpoint contants
#define kTwitterAuthURL             @"https://api.twitter.com/oauth/"
#define kTwitterStatusURL           @"https://api.twitter.com/1/statuses/"
#define kTwitterBaseURL				@"http://api.twitter.com/1"
#define kTwitterSearchURL			@"http://search.twitter.com/search.json"

// Sharing arguments: Page URL, Page Title
#define	kEmailWebShareBodyFormat	@"<b><a href=\"%@\">%@</a></b>"

// iOS UI Element Metrics
#define	kStatusBarHeight            20.0f
#define	kToolbarHeight				44.0f


//TODO: tweak below for Metro
#define kiPadArticleLeftMargin      130
#define kiPadArticleWidthPortrait   464
#define kiPadSidePaneWidth          256
#define	kiPadMainPaneWidth			724 
// Portrait: Screen Width - Nav Bar Width (768-44). 
// Landscape: Screen Width - Nav Bar Width - Side Pane Width (1024-44-256)

#define	kiPadMainViewFrameWidthPortrait     724	// Screen width - Nav bar width (768-44)
#define	kiPadMainViewFrameWidthLandscape	980 // Screen width - Nav bar width (1024-44)
#define kiPadMainViewFrameHeightPortrait    914 // Screen height - Status bar height - Ad height (1024-20-90)
#define kiPadMainViewFrameHeightLandscape   658 // Screen height - Status bar height - Ad height (768-20-90)


// COLORS 
#define kOffwhiteBgColor			[UIColor colorWithRed:0.96470 green:0.95686 blue:0.92941 alpha:1.0]
#define	kDarkerBgColor				[UIColor colorWithRed:0.925 green:0.913 blue:0.894 alpha:1.0]
#define	kDividerColor				[UIColor colorWithRed:0.792 green:0.749 blue:0.718 alpha:1.0]
#define	kTableSeparatorColor		[UIColor colorWithRed:0.875 green:0.824 blue:0.792 alpha:1.0]

#define	kImageOverlayBorderColor	[UIColor colorWithRed:0.8	green:0.768	blue:0.737 alpha:1.0]
#define kiPadHomePageBorderColor    [UIColor colorWithRed:223.0f/255.0f green:210.f/255.0f blue:202.0f/255.0f alpha:1.0f]

// The color (hex string) placed behind ads which are smaller than the ad view size
#define	kAdBackgroundColor			@"CDCDCD"

#define	kDefaultFontColor			[UIColor colorWithRed:0.12941f green:0.12941f blue:0.12941f alpha:1.0f]
#define	kLightFontColor				[UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1.0f]
#define	kDarkTextColor				[UIColor colorWithRed:0.25882f green:0.25882f blue:0.25882f alpha:1.0]


#define kDateFormatString           @"EEEE, MMMM d, yyyy h:mma zzz"

#define kContentUnavailableHtml     @"<html><head><style type='text/css'>body { font-family:GMtext, Georgia, serif; text-align:center; padding: 70px 10px; font-size:16px; } b { color:red; font-size:20px; }</style></head><body><b>Content Unavailable</b><p>Please check your Internet connection and try again.</body></html>"


// Offline constants - In seconds
#define	kOfflineCacheExpiryTime		(7*24*60*60)


// TODO: Tweak for Metro - Font Constants
#define kHomeHeadlinesDefaultFontSize               13.0f
#define kSectionHeadlineDefaultFontSize             20.0f
#define kiPhoneArticleDefaultFontSize               14.0f
#define	kiPadArticleDefaultFontSize					15.0f

#define kHeadlineFontMax                            20.0f
#define kHeadlineFontMin                            8.0f
#define kiPadHeadlineFontMax                        30.0f
#define kiPadHeadlineFontMin                        18.0f

#define kArticleFontMax                             30.0f
#define kArticleFontMin                             12.0f

#define	kInterfaceFontTiny				[UIFont boldInterfaceFontOfSize:11.0f]
#define	kInterfaceFontSmall				[UIFont boldInterfaceFontOfSize:13.0f]
#define	kInterfaceFontMedium			[UIFont boldInterfaceFontOfSize:15.0f]
#define	kInterfaceFontLarge				[UIFont boldInterfaceFontOfSize:17.0f]
#define	kInterfaceFontHuge				[UIFont boldInterfaceFontOfSize:20.0f]
#define	kInterfaceFontGigantic			[UIFont boldInterfaceFontOfSize:24.0f]

// Misc
#define	kLastConnectionTimeKey          @"lastConnectionTime"
#define	kAutoresizingAllMargins			(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)


