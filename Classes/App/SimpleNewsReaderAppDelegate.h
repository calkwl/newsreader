#import <UIKit/UIKit.h>

@interface SimpleNewsReaderAppDelegate : NSObject <UIApplicationDelegate> {
    
    UIWindow *window_;
    UINavigationController *navigationController_;
    UITabBarController *tabBarController_;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;

@end

