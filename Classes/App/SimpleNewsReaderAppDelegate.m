#import "SimpleNewsReaderAppDelegate.h"
#import "ArticleListController.h"


@implementation SimpleNewsReaderAppDelegate

@synthesize window = window_, navigationController = navigationController_, tabBarController = tabBarController_;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    [self.tabBarController setSelectedIndex:1];  // Hardcode to display news tab (assumed index=1) 
    self.navigationController = [[[UINavigationController alloc] initWithRootViewController:self.tabBarController] autorelease];
    navigationController_.navigationBarHidden = YES;
    
	[window_ addSubview:navigationController_.view];
    [window_ makeKeyAndVisible];

    //TODO present Splash screen

	return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

#pragma mark -
#pragma mark Memory management

- (void)dealloc {
    self.navigationController = nil;
    self.window = nil;
    self.tabBarController = nil;
    
	[super dealloc];
}


@end

