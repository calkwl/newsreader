#import <UIKit/UIKit.h>
#import "MWFeedParser.h"
#import "ArticleListCell.h"

@interface ArticleListController : UITableViewController <MWFeedParserDelegate>  {
    ArticleListCell *cellFromNib_;
	MWFeedParser *feedParser;
	NSMutableArray *parsedItems;
	
	NSArray *itemsToDisplay_;
	NSDateFormatter *formatter;
}

@property (nonatomic, retain) NSArray *itemsToDisplay;
@property (nonatomic, assign) IBOutlet ArticleListCell *cellFromNib;

@end
