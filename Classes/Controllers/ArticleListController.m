#import "ArticleListController.h"
#import "Constants.h"
#import "ArticleViewController.h"

@implementation ArticleListController

@synthesize itemsToDisplay = itemsToDisplay_, cellFromNib = cellFromNib_;

static NSString *CellClassName = @"ArticleListCell";


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.title = kLatestNews;
    }
    return self;
}


#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];    

	formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterShortStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];

	parsedItems = [[NSMutableArray alloc] init];
	self.itemsToDisplay = [NSArray array];
	
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh 
																							target:self 
																							action:@selector(refresh)] autorelease];
    NSString *url = kRssUrlBase;
    //  NSString *url = @"http://justintadlock.com/archives/2008/01/27/custom-fields-for-feeds-wordpress-plugin/feed";
    //	NSString *url = @"http://www.metronews.ca/Toronto/world/rss"];
    
    NSURL *feedURL = [NSURL URLWithString:url];
	feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
	feedParser.delegate = self;
	feedParser.feedParseType = ParseTypeFull; 
	feedParser.connectionType = ConnectionTypeAsynchronously;
	[feedParser parse];
}

#pragma mark - Parsing

- (void)refresh {
	[parsedItems removeAllObjects];
	[feedParser stopParsing];
	[feedParser parse];
	self.tableView.userInteractionEnabled = NO;
	self.tableView.alpha = 0.3;
}

- (void)updateTableWithParsedItems {
	self.itemsToDisplay = [parsedItems sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO] autorelease]]];
	self.tableView.userInteractionEnabled = YES;
	self.tableView.alpha = 1;
	[self.tableView reloadData];
}

#pragma mark - MWFeedParserDelegate

- (void)feedParserDidStart:(MWFeedParser *)parser {
	NSLog(@"Started Parsing: %@", parser.url);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedInfo:(MWFeedInfo *)info {
	NSLog(@"Parsed Feed Info: “%@”", info.title);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
	NSLog(@"Parsed Feed Item: “%@”", item.title);
	if (item) [parsedItems addObject:item];	
}

- (void)feedParserDidFinish:(MWFeedParser *)parser {
	NSLog(@"Finished Parsing%@", (parser.stopped ? @" (Stopped)" : @""));
    [self updateTableWithParsedItems];
}

- (void)feedParser:(MWFeedParser *)parser didFailWithError:(NSError *)error {
	NSLog(@"Finished Parsing With Error: %@", error);
    if (parsedItems.count == 0) {
    } else {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Parsing Incomplete"
                                                         message:@"There was an error during the parsing of this feed. Not all of the feed items could parsed."
                                                        delegate:nil
                                               cancelButtonTitle:@"Dismiss"
                                               otherButtonTitles:nil] autorelease];
        [alert show];
    }
    [self updateTableWithParsedItems];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsToDisplay.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleListCell *cell = (ArticleListCell*)[tableView dequeueReusableCellWithIdentifier:CellClassName];
    if (!cell) {
        [[NSBundle mainBundle] loadNibNamed:CellClassName owner:self options:nil];    
        cell = [self.cellFromNib retain];
        self.cellFromNib = nil;
        [cell autorelease];
    }
    
	MWFeedItem *item = [self.itemsToDisplay objectAtIndex:indexPath.row];
	if (item) {		
        [cell refreshWithItem:item];
	}
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MWFeedItem *selectedItem = [self.itemsToDisplay objectAtIndex:indexPath.row];
    ArticleViewController *articleView = [[ArticleViewController alloc] initWithItem:selectedItem];
    [self.navigationController pushViewController:articleView animated:YES];
    [articleView release];	
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (void)viewDidUnload {
    self.itemsToDisplay = nil;    
    self.cellFromNib = nil;
    [super viewDidUnload];
}

- (void)dealloc {
    self.itemsToDisplay = nil;
    self.cellFromNib = nil;
    [formatter release];
	[parsedItems release];
	[feedParser release];
    
    [super dealloc];
}


@end
