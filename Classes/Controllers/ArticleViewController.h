#import <UIKit/UIKit.h>
#import "MWFeedItem.h"

@interface ArticleViewController : UIViewController <UIWebViewDelegate>  {
    
    IBOutlet UIScrollView *scrollView_;
    IBOutlet UITextView *headerView_;
    IBOutlet UIWebView *webView_;
    MWFeedItem *item_;
}

@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UITextView *headerView;
@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) MWFeedItem *item;

- (id)initWithItem:(MWFeedItem*)item;

@end
