#import "ArticleViewController.h"

@interface ArticleViewController (private) 

- (void)disableScrollFor:(UIWebView*)webview;
- (void)calculateWebviewHeight:(UIWebView*)webview;
- (CGSize)getWebviewContentSize:(UIWebView*)webview;
    
@end

    
@implementation ArticleViewController

@synthesize headerView = headerView_, webView = webView_, item = item_, scrollView = scrollView_;

- (id)initWithItem:(MWFeedItem *)item {
    self = [super initWithNibName:@"ArticleViewController" bundle:nil];
    if (self) {
        self.item = item;
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.headerView.text = self.item.title ? self.item.title : @"No Title";
    NSString *htmlString = self.item.content ? self.item.content : self.item.summary;
    
    [self disableScrollFor:self.webView];
    self.webView.delegate = self;
    
    NSURL *baseUrl = self.item.link ? [NSURL URLWithString:self.item.link] : nil;
    [self.webView loadHTMLString:htmlString baseURL:baseUrl];

    
}


#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self calculateWebviewHeight:self.webView];    
}


#pragma mark - Private

- (void)disableScrollFor:(UIWebView *)webview {
    UIScrollView* scrollView = nil;
    for(UIView* v in webview.subviews){
        if([v isKindOfClass:[UIScrollView class] ]){
            scrollView = (UIScrollView*) v;
            scrollView.scrollEnabled = NO;
            scrollView.bounces = NO;
        }
    }
}

- (CGSize)getWebviewContentSize:(UIWebView*)webview {
    CGSize contentSize = CGSizeZero;
    UIScrollView* scrollView = nil;
    for(UIView* v in webview.subviews){
        if([v isKindOfClass:[UIScrollView class] ]){
            scrollView = (UIScrollView*) v;
            contentSize = scrollView.contentSize;
            NSLog(@"content size: %@", NSStringFromCGSize(contentSize));
            return contentSize;
        }
    }    
    return contentSize;
}

- (void)calculateWebviewHeight:(UIWebView *)webView {
    CGRect aFrame = CGRectMake(webView.frame.origin.x, webView.frame.origin.y, webView.frame.size.width, 1);
    webView.frame = aFrame;
    
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    aFrame.size = CGSizeMake(aFrame.size.width, fittingSize.height);
    webView.frame = aFrame;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, webView.frame.origin.y + webView.frame.size.height);
    NSLog(@"webview: %@; scrollView: %@", NSStringFromCGRect(self.webView.frame), NSStringFromCGRect(self.scrollView.frame));
}

- (void)viewDidUnload {
    self.headerView = nil;
    self.webView = nil;
    self.item = nil;
    self.scrollView = nil;
    
    [super viewDidUnload];
}

- (void)dealloc {
    self.headerView = nil;
    self.webView = nil;
    self.item = nil;
    self.scrollView = nil;
    
    [super dealloc];
}

@end
