#import <Foundation/Foundation.h>

// Each photo size has a fixed width, and a standard height (but the height may vary?)
typedef enum {
	PhotoSize2,		// 140(x80) (actually 78 tall? )
	PhotoSize3,		// 220(x125) (actually 123 tall?)
	PhotoSize4,		// 300(x170) (actually 168 tall? or 169? )
	PhotoSize5,		// 380(x215)
	PhotoSize6,		// 460(x260)
	PhotoSize8,		// 620(x350)
	PhotoSizeFull	// 940(x530)
} PhotoSize;


@interface Photo : NSObject <NSCoding> {
    NSString *alt_, *caption_, *copyright_, *url_;
    int height_, width_;
}

@property (nonatomic, retain) NSString *alt;
@property (nonatomic, retain) NSString *caption;
@property (nonatomic, retain) NSString *copyright;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, assign) int height;
@property (nonatomic, assign) int width;

// Get the URL for the image in a particular size
+ (NSString*) urlWithBaseUrl:(NSString*)url forSize:(PhotoSize)size; // Takes in an actual G&M photo URL and mutates it for the requested size
- (NSString*) urlForSize:(PhotoSize)size;
- (id)initWithDictionary:(NSDictionary*)dict;

@end
