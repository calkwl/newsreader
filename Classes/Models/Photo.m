#import "Photo.h"
#import "Utility.h"

@interface Photo ()
+ (NSString*) formatUrlFromBaseUrl:(NSString*)url;
@end


@implementation Photo
@synthesize alt = alt_, caption = caption_, copyright = copyright_, url = url_;
@synthesize height = height_, width = width_;


- (id)initWithDictionary:(NSDictionary*)dict {
    if (self = [super init]) {
        self.alt = [dict objectForKey:@"alt"];
        self.caption = [[dict objectForKey:@"caption"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        self.copyright = [dict objectForKey:@"copyright"];
		
		NSString* url = [dict objectForKey:@"url"];
		if (url) {
			// Remove the "cl-x" string from just before the file extension
			url_ = [[Photo formatUrlFromBaseUrl:url] retain];
		}
				
        width_ = [[dict objectForKey:@"width"] integerValue];
        height_ = [[dict objectForKey:@"height"] integerValue];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
	self = [super init];
	
	self.alt = [aDecoder decodeObjectForKey:@"photo_alt"];
	self.caption = [aDecoder decodeObjectForKey:@"photo_caption"];
	self.copyright = [aDecoder decodeObjectForKey:@"photo_copyright"];
	self.url = [aDecoder decodeObjectForKey:@"photo_url"];
	self.width = [aDecoder decodeIntForKey:@"photo_width"];
	self.height = [aDecoder decodeIntForKey:@"photo_height"];
	
	return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
	if (alt_) {[aCoder encodeObject:alt_ forKey:@"photo_alt"];}
	if (caption_) {[aCoder encodeObject:caption_ forKey:@"photo_caption"];}
	if (copyright_) {[aCoder encodeObject:copyright_ forKey:@"photo_copyright"];}
	if (url_) {[aCoder encodeObject:url_ forKey:@"photo_url"];}
	if (width_>0) {[aCoder encodeInt:width_ forKey:@"photo_width"];}
	if (height_>0) {[aCoder encodeInt:height_ forKey:@"photo_height"];}
}

#pragma mark -
#pragma mark Photo Size methods

+ (NSString*) stringForPhotoSize:(PhotoSize)size {
	switch (size) {
		case PhotoSize2:
			return @"cl-2";
		case PhotoSize3:
			return @"cl-3";
		case PhotoSize4:
			return @"cl-4";
		case PhotoSize5:
			return @"cl-5";
		case PhotoSize6:
			return @"cl-6";
		case PhotoSize8:
			return @"cl-8";
		case PhotoSizeFull:
			return @"cl-f";
		default:
			return @"cl-2";
	}
}

+ (PhotoSize) convertPhotoSize:(PhotoSize)size {
	static const PhotoSize photoSizeConversions[] = {PhotoSize4, PhotoSize6, PhotoSize8, PhotoSizeFull, PhotoSizeFull, PhotoSizeFull, PhotoSizeFull};
	
	// If the device has a double-res (retina) screen, choose a more appropriate size for it
	if ([Utility hasHighResScreen]) {
		return photoSizeConversions[size];
	}
	else {
		return size;
	}
}

+ (NSString*) urlWithUrlFormat:(NSString*)url forSize:(PhotoSize)size {
	return [NSString stringWithFormat:url, [Photo stringForPhotoSize:[Photo convertPhotoSize:size]]];
}

+ (NSString*) formatUrlFromBaseUrl:(NSString*)url {
    //Old logic for GlobeAndMail images
//	if ([url length] > 8) {
//		return [url stringByReplacingCharactersInRange:NSMakeRange([url length] - 8, 4) withString:@"%@"];
//	}
//	else {
//		return url;
//	}
    return url;  
}

+ (NSString*) urlWithBaseUrl:(NSString*)url forSize:(PhotoSize)size {
    //Old logic for GlobeAndMail
//	return [Photo urlWithUrlFormat:[Photo formatUrlFromBaseUrl:url] forSize:size];
    return url;
}

- (NSString*) urlForSize:(PhotoSize)size {
    //Old logic for GlobeAndMail
//	return [Photo urlWithUrlFormat:url_ forSize:size];
    return url_;
}

#pragma mark -

- (void)dealloc {
    [alt_ release]; alt_ = nil;
    [caption_ release]; caption_ = nil;
    [copyright_ release]; copyright_ = nil;
    [url_ release]; url_ = nil;
    [super dealloc];
}

@end
