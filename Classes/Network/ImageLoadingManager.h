#import <Foundation/Foundation.h>

@protocol ImageLoadingManagerDelegate;
@class ImageUrlConnection;

@interface ImageLoadingManager : NSObject {
	NSMutableArray *_connections;		// In-progress connections
	NSMutableArray *_queuedConnections;	// Waiting connections
}

+ (ImageLoadingManager*) sharedInstance;

- (void) reset;

- (ImageUrlConnection*) loadImage:(NSString*)url delegate:(id<ImageLoadingManagerDelegate>) del;
- (void) cancelLoadingImageConnection:(ImageUrlConnection*)connection;

- (BOOL) hasOutstandingRequests;

@end


@protocol ImageLoadingManagerDelegate

- (void) finishedLoadingImage: (UIImage*)image fromUrl:(NSString*)url connection:(ImageUrlConnection*)conn;
- (void) failedLoadingImageFromUrl:(NSString*)url connection:(ImageUrlConnection*)conn;

@end