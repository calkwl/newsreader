#import <Foundation/Foundation.h>
#import "ImageLoadingManager.h"


@interface ImageUrlConnection : NSURLConnection {

}

@property (nonatomic, assign) id<ImageLoadingManagerDelegate> requesterDelegate;
@property (nonatomic, retain) NSString *sourceUrl;
@property (nonatomic, retain) NSMutableData *receivedData;

@end
