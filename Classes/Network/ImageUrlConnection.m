#import "ImageUrlConnection.h"


@implementation ImageUrlConnection

@synthesize requesterDelegate, sourceUrl, receivedData;

- (void) dealloc {
	[sourceUrl release];
	[receivedData release];
	[super dealloc];
}

@end
