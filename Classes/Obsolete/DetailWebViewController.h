#import <UIKit/UIKit.h>
#import "MWFeedItem.h"

@interface DetailWebViewController : UITableViewController {
    MWFeedItem *_item;
    NSString *_dateString, *_htmlString;
}

@property (nonatomic, retain) MWFeedItem *item;
@property (nonatomic, retain) NSString *dateString, *htmlString;


@end

