#import "DetailWebViewController.h"
#import "NSString+HTML.h"
#import "WebViewCell.h"

typedef enum { SectionHeader, SectionDetail } Sections;
typedef enum { SectionDetailSummary } DetailRows;
typedef enum { HeaderTitle, HeaderDate, HeaderURL } HeaderRows;

@implementation DetailWebViewController

@synthesize dateString = _dateString, item = _item, htmlString = _htmlString;

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

	if (self.item.date) {
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateStyle:NSDateFormatterMediumStyle];
		[formatter setTimeStyle:NSDateFormatterMediumStyle];
		self.dateString = [formatter stringFromDate:self.item.date];
		[formatter release];
	}
	
	// Summary
	if (self.item.summary) {
		self.htmlString = self.item.summary;
	} 
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 3;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
	if (self.item) {		
        if (indexPath.section == 0) {
            static NSString *CellIdentifier = @"HeaderCell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }        
            cell.textLabel.textColor = [UIColor darkGrayColor];
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            
            NSString *itemTitle = self.item.title ? [self.item.title stringByConvertingHTMLToPlainText] : @"[No Title]";		
            //Header cell with meta data
            switch (indexPath.row) {
                case HeaderTitle:
                    cell.textLabel.font = [UIFont systemFontOfSize:14];
                    cell.textLabel.text = itemTitle;
                    break;
                    
                case HeaderDate:
                    cell.textLabel.text = self.dateString ? self.dateString : @"[No Date]";
                    break;
                case HeaderURL:
                    cell.textLabel.text = self.item.link ? self.item.link : @"[No Link]";
                    cell.textLabel.textColor = [UIColor blueColor];
                    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    break;
                    
                default:
                    break;
            }
        }
        else {
            // Web view cell with html content
            static NSString *CellIdentifier = @"WebViewCell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[[WebViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            if (self.htmlString) {
                WebViewCell *contentCell = (WebViewCell*) cell;
                NSURL *baseUrl = [NSURL URLWithString:self.item.link ? self.item.link : @"www.metronews.ca"];
                [contentCell.webview loadHTMLString:self.htmlString baseURL:baseUrl];
                
                CGSize fitSize = [contentCell.webview sizeThatFits:contentCell.frame.size];  //290, 320?
//                CGSize webviewSize = contentCell.webview.frame.size;
                NSLog(@"Webview size: %@", NSStringFromCGSize(fitSize));
            }
        }
	}    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SectionHeader) {
		return 34;		
	} else {
		return 480;

//		NSString *summary = @"[No Summary]";
//		if (summaryString) summary = summaryString;
//		CGSize s = [summary sizeWithFont:[UIFont systemFontOfSize:15] 
//					   constrainedToSize:CGSizeMake(self.view.bounds.size.width - 40, MAXFLOAT)  // - 40 For cell padding
//						   lineBreakMode:UILineBreakModeWordWrap];
//		return s.height + 16; // Add padding
		
	}
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == SectionHeader && indexPath.row == HeaderURL) {
		if (self.item.link) {
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.item.link]];
		}
	}
	
	// Deselect
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];

    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

#pragma mark -
#pragma mark Memory management

- (void)dealloc {
    self.item = nil;
    self.dateString = nil;
    self.htmlString = nil;
    
    [super dealloc];
}

@end
