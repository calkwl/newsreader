#import <UIKit/UIKit.h>

@interface WebViewCell : UITableViewCell {
    UIWebView *_webview;
}

@property (nonatomic, retain) UIWebView *webview;


@end
