#import "WebViewCell.h"

#define OFFSET 15.0f

@implementation WebViewCell

@synthesize webview = _webview;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat width = self.frame.size.width;
        self.webview = [[[UIWebView alloc] initWithFrame:CGRectMake(OFFSET, OFFSET, width - 2 * OFFSET, 320)] autorelease];  
        [self addSubview:self.webview];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

#pragma mark -
#pragma mark Memory management

- (void)dealloc {
    self.webview = nil;
    [super dealloc];
}

@end
