//
//  Utility.h
//  CalorieCount
//
//  Created by Xtreme Labs on 09-11-16.
//  Copyright 2009 Xtreme Labs Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
#import <CoreLocation/CoreLocation.h>

@class BasicArticle;

@interface Utility : NSObject {
}

+ (NSString *)hmacsha1:(NSString *)data key:(NSString *)key;
+ (NSString*)encodeWithPercentEscapes:(NSString*)str;
+ (BOOL)resultOK:(id)object;
+ (int)checkResultForErrors:(id)object;
+ (int)checkDictionaryForErrorCode:(NSDictionary*)result;
+ (NSDictionary*)buildServerResponse:(NSString*)response;
+ (NSDictionary*)buildPlainTextResponse:(NSString*)response;

+ (UIColor *) colorForHex:(NSString *)hexColor;

+ (UIColor*) tiledAppBackground;
+ (UIColor*) tiledAppBackgroundDark;
+ (UIColor*) tiledAppSideViewBackground;

+ (NSDate*) dateForToday; // Returns an NSDate object for today's date with a time of 00:00
+ (NSDateFormatter*) dateFormatterForArticle;
+ (NSString*) stringForTimePassedFrom:(NSDate*)startDate until:(NSDate*)endDate;

+ (BOOL) hasHighResScreen;

// If the given URL links to a Globe&Mail article, returns an article ID
// corresponding to the article from the URL. If it is an ordinary link
// external to the Globe, returns NSNotFound
+ (NSInteger) articleFromURL:(NSURL*)url;

+ (NSString*)getUrlForTwitterKeyword:(NSString*)keyword;
+ (NSArray*) createRangesWithBody:(NSString*)body;
@end
