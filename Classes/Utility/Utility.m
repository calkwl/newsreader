#import <math.h>
#import "Utility.h"
#import "NSData+Additions.h"
#import "JSON.h"

@implementation Utility

+ (NSString *)sha1:(NSString *)str {
    unsigned char hashedChars[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1([str UTF8String], [str lengthOfBytesUsingEncoding:NSUTF8StringEncoding], hashedChars);
	NSMutableString *hexString = [[[NSMutableString alloc] init] autorelease];
	for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [hexString appendFormat:@"%02x", (unsigned char)(hashedChars[i])];   
    }
    return hexString;
}

+ (NSString *)hmacsha1:(NSString *)data key:(NSString *)key {
    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMAC = [[[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)] autorelease];
    return [HMAC base64Encoding];
}

+ (NSString*)encodeWithPercentEscapes:(NSString*)str {
	if (!str) { str = @""; }
	NSString *s = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)str, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[] ", kCFStringEncodingUTF8);
    return [s autorelease];
}


+ (BOOL)resultOK:(id)object {
    if (object == nil || [[object objectForKey:@"HTTPResponseStatus"] intValue] != 200) {
        return NO;
    }
    return YES;
}

+ (int)checkResultForErrors:(id)object {
    if (object == nil) {
        //[[Utility notificationUtility] showNotification:kStringError message:kStringErrorConnection];
        return 1;
    } else {
        int status;
		if ([object isKindOfClass:[NSDictionary class]]) {
			status = [[object objectForKey:@"HTTPResponseStatus"] intValue];
		}
		else {
			status = [object code];
		}
        if (status >= 500) {
            id result = [object objectForKey:@"result"];
            if ([result isKindOfClass:[NSArray class]] && [result count] > 0) {
                return [Utility checkDictionaryForErrorCode:[result objectAtIndex:0]];
            } else if ([result isKindOfClass:[NSDictionary class]]) {
                return [Utility checkDictionaryForErrorCode:result];                 
            } else {
                //[[Utility notificationUtility] showNotification:kStringError message:kStringErrorServer];
                return 1;
            }
        } else if (status >= 400 || status == -1009) {
            return status;
        }
    }
    return 0;
}

+ (int)checkDictionaryForErrorCode:(NSDictionary*)result {
    if ([result objectForKey:@"code"]) {
        //NSString *description = [result objectForKey:@"desc"];
        //[[Utility notificationUtility] showNotification:kStringError message:description];
        return 1;
    }
    return 0;
}

+ (NSDictionary*)buildServerResponse:(NSString*)response {
	id object;
    SBJSON *parser = [[[SBJSON alloc] init] autorelease];
	if ((object = [parser objectWithString:response error:nil])) {
        return [NSDictionary dictionaryWithObject:object forKey:@"result"];
    }
    return [self buildPlainTextResponse:response];
}

+ (NSDictionary*)buildPlainTextResponse:(NSString*)response {
    NSMutableDictionary *oauth = [NSMutableDictionary dictionary];
    NSArray *data = [response componentsSeparatedByString:@"&"];
    for (NSString *pairs in data) {
        NSArray *keyValue = [pairs componentsSeparatedByString:@"="];
        if ([keyValue count] > 1){
            [oauth setObject:[keyValue objectAtIndex:1] forKey:[keyValue objectAtIndex:0]];
        } else if ([data count] == 1 && [keyValue count] == 1) {
            [oauth setObject:response forKey:@"error"];
        }
    }
    return oauth;
}


+ (UIColor *) colorForHex:(NSString *)hexColor {
	hexColor = [[hexColor stringByTrimmingCharactersInSet:
				 [NSCharacterSet whitespaceAndNewlineCharacterSet]
				 ] uppercaseString];
	
    if ([hexColor length] != 6) 
		return nil;//[UIColor blackColor];
	
    // Separate into r, g, b substrings  
    NSRange range;  
    range.location = 0;  
    range.length = 2; 
	
    NSString *rString = [hexColor substringWithRange:range];  
	
    range.location = 2;  
    NSString *gString = [hexColor substringWithRange:range];  
	
    range.location = 4;  
    NSString *bString = [hexColor substringWithRange:range];  
	
    // Scan values  
    unsigned int r, g, b;  
    [[NSScanner scannerWithString:rString] scanHexInt:&r];  
    [[NSScanner scannerWithString:gString] scanHexInt:&g];  
    [[NSScanner scannerWithString:bString] scanHexInt:&b];  
	
    return [UIColor colorWithRed:((float) r / 255.0f)  
                           green:((float) g / 255.0f)  
                            blue:((float) b / 255.0f)  
                           alpha:1.0f];  
}


static UIColor * tiledBgColor;
static UIColor * tiledBgColorDark;
static UIColor * tiledBgColorSideView;

+ (UIColor*) tiledAppBackground {
#ifndef USE_SOLID_BACKGROUND
	if (!tiledBgColor) { tiledBgColor = [[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]] retain]; }
#else
	if (!tiledBgColor) { tiledBgColor = [kOffwhiteBgColor retain]; }
#endif
	return tiledBgColor;
}

+ (UIColor*) tiledAppBackgroundDark {
#ifndef USE_SOLID_BACKGROUND
	if (!tiledBgColorDark) { tiledBgColorDark = [[UIColor colorWithPatternImage:[UIImage imageNamed:@"background_dark.png"]] retain]; }
#else
	if (!tiledBgColorDark) { tiledBgColorDark = [kDarkerBgColor retain]; }
#endif
	return tiledBgColorDark;
}

+ (UIColor*) tiledAppSideViewBackground {
#ifndef USE_SOLID_BACKGROUND
	if (!tiledBgColorSideView) { tiledBgColorSideView = [[UIColor colorWithPatternImage:[UIImage imageNamed:@"side-view-background-iPad.png"]] retain]; }
#else
	if (!tiledBgColorSideView) { tiledBgColorSideView = [kDarkerBgColor retain]; }
#endif
	return tiledBgColorSideView;    
}

static NSDateFormatter *utilityFormatter;
+ (NSDateFormatter*) dateFormatterForDays {
	if (!utilityFormatter) {
		utilityFormatter = [[NSDateFormatter alloc] init];
		[utilityFormatter setDateFormat:@"yyyy-MM-dd"];
	}
	return utilityFormatter;
}

static NSDateFormatter *articleDateFormatter;
+ (NSDateFormatter*) dateFormatterForArticle {
	if (!articleDateFormatter) {
		articleDateFormatter = [[NSDateFormatter alloc] init];
		[articleDateFormatter setDateFormat:@"EEEE, MMMM d, yyyy h:mma zzz"];  //TODO: Put this in a Constant class
	}
	return articleDateFormatter;
}

+ (NSDate*) dateForToday {
	NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
	
	NSDateFormatter *formatter = [Utility dateFormatterForDays];
	return [formatter dateFromString:[formatter stringFromDate:date]];
}

+ (NSString*) stringForTimePassedFrom:(NSDate*)startDate until:(NSDate*)endDate {
	NSCalendar *sysCalendar = [NSCalendar currentCalendar];
	unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
	NSDateComponents *conversionInfo = [sysCalendar components:unitFlags fromDate:startDate toDate:endDate options:0];
	
	int seconds = [conversionInfo second];
	int minutes = [conversionInfo minute];
	int hours = [conversionInfo hour];
	int days = [conversionInfo day];
	int months = [conversionInfo month];
	
	if (months == 0 && days == 0 && hours == 0 && minutes == 0) {
		return (seconds == 1) ? @"1 sec" : [NSString stringWithFormat:@"%d secs", seconds];
	} else if (months == 0 && days == 0 && hours == 0) {
		return (minutes == 1) ? @"1 min" : [NSString stringWithFormat:@"%d mins", minutes];
	} else if (months == 0 && days == 0) {
		return (hours == 1) ? @"1 hour" : [NSString stringWithFormat:@"%d hours", hours];
	} else if (months == 0) {
		return (days == 1) ? @"1 day" : [NSString stringWithFormat:@"%d days", days];
	} else {
		return (months == 1) ? @"1 month" : [NSString stringWithFormat:@"%d months", months];
	}
}

static BOOL checkedScreenSize = NO;
static BOOL hasHighResScreen = NO;

+ (BOOL) hasHighResScreen {
	if (!checkedScreenSize) {
		if ([UIScreen instancesRespondToSelector:@selector(scale)]) {
			CGFloat scale = [[UIScreen mainScreen] scale];
			if (scale > 1.0) {
				hasHighResScreen = YES;
			}
		}
		checkedScreenSize = YES;
	}
	
	return hasHighResScreen;
}


+ (NSInteger) articleFromURL:(NSURL*)url {
	// Looks for a URL of the following format, and extracts the article ID from it:
	// http://www.theglobeandmail.com/report-on-business/economy/gdp-gains-yet-to-bite-into-jobless-rate/article1923213/
	
	NSString *str = [url absoluteString];
	NSLog(@"Checking URL: %@", str);
	
	NSArray *components = [str componentsSeparatedByString:@"/"];
	NSLog(@"URL components: %@", components);
	NSString *articleComp = @"";
	
	if ([components count] >= 2) {
		articleComp = [components objectAtIndex:[components count]-2];
		NSLog(@"Article component: %@", articleComp);
	}
	
	if ([articleComp rangeOfString:@"article"].location == 0) {
		NSInteger articleID = [[articleComp substringFromIndex:7] integerValue];
		if (articleID != 0) {
			NSLog(@"Found article ID: %d", articleID);
			return articleID;
		}
	}
	
	return NSNotFound;
}

+ (NSString*)getUrlForTwitterKeyword:(NSString*)keyword {
	if ((keyword == nil) || ([keyword length] < 2)) return @"";
	keyword = [keyword stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	NSString *url;
	if ([keyword hasPrefix:@"#"]) {
		url = [@"http://mobile.twitter.com/search?q=%23" stringByAppendingString:[keyword substringFromIndex:1]];
	} else if ([keyword hasPrefix:@"@"]) {
		url = [@"http://mobile.twitter.com/" stringByAppendingString:[keyword substringFromIndex:1]];
	} else {
		url = keyword;
	}
	return url;
}


static const int pageBreakCharacterCount = 3000;

+ (NSArray*) createRangesWithBody:(NSString*)body {
	NSMutableArray *ranges = [NSMutableArray array];
	
	if (!body || [body length] == 0) { return ranges; }
	
	// First look for the break where Related Stories belongs
	NSRange range = [body rangeOfString:@"<!-- brick location -->"];
	
	if (range.location != NSNotFound) {
		[ranges addObject:[NSValue valueWithRange:NSMakeRange(0, range.location)]];
	}
	else {
		range = NSMakeRange(0, 0); // Reset range to the beginning for starting the rest of the search
	}
	
	//[ranges addObject:[NSValue valueWithRange:NSMakeRange(range.location+range.length, [body length]-range.location-range.length)]];
	
	NSInteger startIndex = range.location+range.length;
	while (true) {
		NSInteger endIndex = startIndex;
		
		while (endIndex-startIndex <= pageBreakCharacterCount && endIndex != NSNotFound) {
			endIndex = [body rangeOfString:@"</p>" options:0 range:NSMakeRange(endIndex, [body length]-endIndex)].location;
			if (endIndex != NSNotFound) {
				endIndex += 4; // Include the </p> tag in the range
			}
		}
		
		if (endIndex == NSNotFound) {
			if (startIndex < [body length]) {
				// Add the final range to the array
				[ranges addObject:[NSValue valueWithRange:NSMakeRange(startIndex, [body length]-startIndex)]];
			}
			break;
		}
		else {
			[ranges addObject:[NSValue valueWithRange:NSMakeRange(startIndex, endIndex-startIndex)]];
			startIndex = endIndex;
		}
	}
	
	/*// Now search for end-of-page markers in the body, starting from the previous search hit
	NSInteger startIndex = range.location+range.length;
	while ((range = [body rangeOfString:@"<!-- end of page [0-9]* -->" options:NSRegularExpressionSearch range:NSMakeRange(startIndex, [body length]-startIndex)]).location != NSNotFound) { 
		NSRange newRange = NSMakeRange(startIndex, range.location-startIndex);
		[ranges addObject:[NSValue valueWithRange:newRange]];
		
		startIndex = range.location+range.length;
	}
	
	// Add one more range to take care of any final characters
	if (startIndex < [body length]-1) {
		[ranges addObject:[NSValue valueWithRange:NSMakeRange(startIndex, [body length]-startIndex)]];
	}*/
	
	return ranges;
}


@end
