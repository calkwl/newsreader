#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "MWFeedItem.h"

@interface ArticleListCell : UITableViewCell {
    
    AsyncImageView *thumbnailView_;
    IBOutlet UILabel *headlineView_;
    IBOutlet UILabel *timeView_;
}

@property (nonatomic, retain) AsyncImageView *thumbnailView;
@property (nonatomic, retain) UILabel *headlineView, *timeView;

- (void)refreshWithItem:(MWFeedItem*)newItem;

@end
