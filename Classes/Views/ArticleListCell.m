#import "ArticleListCell.h"
#import "NSString+HTML.h"

@interface ArticleListCell (private)
- (NSString *)getFirstThumbnailUrl:(MWFeedItem *)item;
@end


@implementation ArticleListCell

@synthesize thumbnailView = thumbnailView_, headlineView = headlineView_, timeView = timeView_;

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        self.thumbnailView = [[[AsyncImageView alloc] initWithFrame:CGRectMake(3, 3, 60, 60)] autorelease];
        [self addSubview:self.thumbnailView];
    }
    return self;
}


#pragma mark - Public methods

- (void)refreshWithItem:(MWFeedItem *)newItem {
    NSString *itemTitle = newItem.title ? [newItem.title stringByConvertingHTMLToPlainText] : @"[No Title]";
    NSString *thumbnailUrl = [self getFirstThumbnailUrl:newItem];
    
    if (thumbnailUrl) {
        [self.thumbnailView loadImageFromUrl:thumbnailUrl];
    }

    self.headlineView.text = itemTitle;
    
    if (newItem.date) {
        //TODO do not do this for each cell!
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterShortStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        self.timeView.text = [formatter stringFromDate:newItem.date];
        [formatter release];
    }
}


#pragma mark - Private

- (NSString *)getFirstThumbnailUrl:(MWFeedItem *)item {
    NSArray *enclosures = item.enclosures;
    if ([enclosures count]) {
        for (NSDictionary *enclosure in enclosures) {
            NSString *type = [enclosure valueForKey:@"type"];
            if (type && [[type lowercaseString] isEqualToString:@"image"]) {
                NSString *url = [enclosure valueForKey:@"url"];
                
                // Exclude the avatar image (WordPress-specific?)
                if (url && [url rangeOfString:@"avatar"].location == NSNotFound) {                 
                    return url;
                }
            }
        }
    }
    return nil;        
}

- (void)dealloc {
    self.thumbnailView = nil;
    self.headlineView = nil;
    self.timeView = nil;
    
    [super dealloc];
}

@end
