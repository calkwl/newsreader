#import <UIKit/UIKit.h>
#import "ImageLoadingManager.h"

@protocol AsyncImageViewDelegate;

@interface AsyncImageView : UIImageView <ImageLoadingManagerDelegate> {
	NSString *_requestedUrl;
	ImageUrlConnection *_connection;
	
	UIColor *_overlayBackgroundColor;
	UIView *_overlayView;
}

@property (nonatomic, assign) id<AsyncImageViewDelegate> delegate;

@property (nonatomic) BOOL loaded;

@property (nonatomic, readonly) UIView *overlayView;

@property (nonatomic) BOOL overlayBorder; // Whether to show a border around the placeholder overlay. Default is YES

- (void) loadImageFromUrl:(NSString*)url;
- (void) clearImage; // Get rid of any image being shown and show the overlay instead
@end

@protocol AsyncImageViewDelegate <NSObject>

- (void) imageDidLoad:(AsyncImageView*)image;
- (void) imageFailedToLoad:(AsyncImageView*)image;

@optional
- (BOOL) imageShouldFadeIn:(AsyncImageView*)image;
@end