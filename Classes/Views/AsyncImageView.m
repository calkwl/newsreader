#import <QuartzCore/QuartzCore.h>
#import "AsyncImageView.h"
#import "ImageCacheManager.h"
#import "Utility.h"

#define PLACEHOLDER_TAG 9876

@interface AsyncImageView (Private)
- (void) loadImage:(NSString*)img;
- (void) insertImage: (NSDictionary*) data;
@end

@implementation AsyncImageView

@synthesize loaded, delegate, overlayView = _overlayView, overlayBorder;

- (id)init {
	return [self initWithFrame:CGRectMake(0, 0, 100, 100)];
}

- (id) initWithFrame:(CGRect)frame {
	if ((self = [super initWithFrame:frame])) {
		_overlayBackgroundColor = [[Utility tiledAppBackground] retain];
		self.overlayBorder = YES;
	}
	return self;
}

- (void) setBackgroundColor:(UIColor *)color {
	if (color != _overlayBackgroundColor) {
		[_overlayBackgroundColor release];
		_overlayBackgroundColor = [color retain];
		
		_overlayView.backgroundColor = color;
	}
}

- (void) setOverlayBorder:(BOOL)show {
	overlayBorder = show;
	_overlayView.layer.borderWidth = show ? 1 : 0;
}

- (void) layoutOverlayLogo:(UIView*)logo {
	CGFloat size = 113.0f; // The native size of the logo image
	CGFloat padding = 10.0f;
	
	if (self.frame.size.height < size+(padding*2)) {
		size = self.frame.size.height-(padding*2);
	}
	if (self.frame.size.width < size+(padding*2)) {
		size = self.frame.size.width-(padding*2);
	}
	
	logo.bounds = CGRectMake(0, 0, size, size);
	logo.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
}

- (void) layoutSubviews {
	[super layoutSubviews];
	
	if (_overlayView) {
		// Fix the layout of the placeholder logo
		[self layoutOverlayLogo:[_overlayView viewWithTag:PLACEHOLDER_TAG]];
	}
}

#pragma mark -

- (void) removeOverlayView {
	if (_overlayView) {
		[_overlayView removeFromSuperview];
		[_overlayView release];
		_overlayView = nil;
	}
}

- (void) setupOverlayView {
	if (!_overlayView) {
		_overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
		_overlayView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
		_overlayView.backgroundColor = _overlayBackgroundColor;
        _overlayView.contentMode = UIViewContentModeScaleAspectFill;
		CALayer *layer = [_overlayView layer];
		layer.borderWidth = overlayBorder ? 1 : 0;
		layer.borderColor = [UIColor colorWithRed:0.8	green:0.768	blue:0.737 alpha:1.0].CGColor;  //kImageOverlayBorderColor
		
		UIImageView *defaultLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"metro_default.png"]];
		defaultLogoView.tag = PLACEHOLDER_TAG;
        [self layoutOverlayLogo:defaultLogoView];
		[_overlayView addSubview:defaultLogoView];
        [defaultLogoView release];
	}
	
	[_overlayView removeFromSuperview];
	_overlayView.alpha = 1.0;
	[self addSubview:_overlayView];
}

- (void) fadeOutOverlay {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(removeOverlayView)];
	_overlayView.alpha = 0.0;
	[UIView commitAnimations];
}

- (void) loadImageFromUrl:(NSString*)url {
	if (_connection) { // If there is a connection in progress
		[[ImageLoadingManager sharedInstance] cancelLoadingImageConnection:_connection];
		[_connection release];
		_connection = nil;		
	}
	
	if ([[ImageCacheManager sharedInstance] getImageFromCacheForUrl:url]) {
		self.image = [[ImageCacheManager sharedInstance] getImageFromCacheForUrl:url];
		[self removeOverlayView]; // Remove the overlay instantly if we already have the image
		
		self.loaded = YES;
		[self.delegate imageDidLoad:self];
	}
	else {
		self.loaded = NO;
		
		[_requestedUrl release];
		_requestedUrl = [url retain];
		
		self.image = nil;
		[self setupOverlayView];
        
        if (!url || [url length] == 0) { return; }
        
		_connection = [[[ImageLoadingManager sharedInstance] loadImage:_requestedUrl delegate:self] retain];
	}
}

- (void) clearImage {
	self.image = nil;
	[self setupOverlayView];
}
                            
#pragma mark -
#pragma mark Callbacks After Load Success/Failure

- (void) imageLoadedSuccessfully:(UIImage*)img fromUrl:(NSString*)url {
	if ([url isEqualToString:_requestedUrl]) {
		self.image = img;
		
		if (![delegate respondsToSelector:@selector(imageShouldFadeIn:)] || [delegate imageShouldFadeIn:self]) {
			[self fadeOutOverlay];
		}
		else {
			[self removeOverlayView];
		}
		
		[_requestedUrl release];
		_requestedUrl = nil;
		
		self.loaded = YES;
		[self.delegate imageDidLoad:self];
	}
}
- (void) imageLoadedSuccessfully:(NSDictionary*)info {
	[self imageLoadedSuccessfully:[info objectForKey:@"image"] fromUrl:[info objectForKey:@"url"]];
}

- (void) imageFailedToLoadFromUrl:(NSString*)url {
	[_requestedUrl release];
	_requestedUrl = nil;
	
	[self.delegate imageFailedToLoad:self];
}

#pragma mark -
#pragma mark Delegate Methods

- (void) finishedLoadingImage: (UIImage*)img fromUrl:(NSString*)url connection:(ImageUrlConnection*)conn {
	// Cache the image as-is
	[[ImageCacheManager sharedInstance] addImage:img toCacheForUrl:url];
	
	[_connection release];
	_connection = nil;
	
	[self imageLoadedSuccessfully:img fromUrl:url];
}

- (void) failedLoadingImageFromUrl:(NSString *)url connection:(ImageUrlConnection*)conn {
	[_connection release];
	_connection = nil;
	
	// If the image didn't load from the server, try loading it from disk (on a background thread)!
	[self performSelectorInBackground:@selector(loadFromDiskCacheForUrl:) withObject:url];
}

- (void) loadFromDiskCacheForUrl:(NSString*)url {
	// CALLED ON BACKGROUND THREAD!
	
	// We need an autorelease pool for all this stuff...
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	// Try loading the URL from the disk cache. If it isn't there, try loading it in various sizes as fallbacks
	UIImage *img = [[ImageCacheManager sharedInstance] getImageFromDiskCacheForUrl:url];
	
	if(!img) {
        //Old logic for GlobeAndMail with multiple image sizes
//		for (int sizeToTry = PhotoSizeFull; sizeToTry >= 0; sizeToTry--) {
//			img = [[ImageCacheManager sharedInstance] getImageFromDiskCacheForUrl:[Photo urlWithBaseUrl:url forSize:sizeToTry]];
//			if (img) {
//				break;
//			}
//		}
        img = [[ImageCacheManager sharedInstance] getImageFromDiskCacheForUrl:url];
	}
	
	if (img) {
		[self performSelectorOnMainThread:@selector(imageLoadedSuccessfully:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:img,@"image", url,@"url", nil] waitUntilDone:YES];
	}
	else {
		[self performSelectorOnMainThread:@selector(imageFailedToLoadFromUrl:) withObject:url waitUntilDone:YES];
	}
	
	[pool release];
}

- (void) dealloc {
	[_requestedUrl release];
	
	[[ImageLoadingManager sharedInstance] cancelLoadingImageConnection:_connection];
	[_connection release];
	
	[_overlayBackgroundColor release];
	[_overlayView release];
	[super dealloc];
}

@end
